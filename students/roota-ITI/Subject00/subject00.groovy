/*
public class subject00
{
	public static void main(String[] args) {
		System.out.println("Hello Groovy World!");
	}
}

// 改行あり
println 'Hello Groovy World!'

// 改行なし
print 'Hello Groovy World!'
*/
strHelloWorld = 'Hello Groovy World!'
strHello = 'Hello'
// ダブルクォート、シングルクォートの違い
//    変数展開できる
println "${strHello} Groovy World!"
//    変数展開できない
println '${strHello} Groovy World!'
