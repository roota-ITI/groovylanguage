// HTTP Client
// 
// サーバー検証用にクライアントを作成
//
// telnet localhost 8080

import java.net.*;

	Socket sock = new Socket("localhost", 8080);

	InputStream is = sock.getInputStream();

	OutputStream os = sock.getOutputStream();

	// 要求を送信
	os.write("GET /index.html HTTP/1.0\r\n".getBytes());
    os.write("\r\n".getBytes());
	os.flush();

	// サーバーからのメッセージを取得して表示
/*  1文字づつ表示するパターン
	InputStreamReader isr = new InputStreamReader(is);

	// 応答を受信
	int i = isr.read();
	while(i != -1) {
		println((char)i);
		i = isr.read();
	}
*/

	BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	println('response = ' + reader.readLine());

	sock.close();
