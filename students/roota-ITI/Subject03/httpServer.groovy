// Simple HTTP server
// 
// http://localhost/ でアクセスすると、
// 「 Welcome to my site.」と表示する
// lhost/ とアクセスしても、 http://localhost/xyz とアクセスしても
// 同じ結果になる
//
// telnet localhost 8080

import java.net.*;

ServerSocket server = new ServerSocket(8080);

try {
	while(true) {
		// クライアントからの接続を待つ
		// accept() の戻りは Socket
		Socket connect = server.accept();

		// /127.0.0.1:48353
//		SocketAddress clientinfo = connect.getRemoteSocketAddress();
		// /127.0.0.1:8080
		SocketAddress clientinfo = connect.getLocalSocketAddress();
		// vmRHEL5.7x64
//		String clientinfo = connect.getInetAddress().getHostName();

		println('connect ' + clientinfo);

		// クライアントからのメッセージを取得して表示
		BufferedReader reader = new BufferedReader
			(new InputStreamReader(connect.getInputStream()));
		println('request = ' + reader.readLine());

		/* 分割して表示
		String[] words = reader.readLine().split(" ");
		for(String w : words) {
			println(w);
		}
		*/

		// クライアントへメッセージを送信
		OutputStreamWriter out =
			new OutputStreamWriter(connect.getOutputStream());
		out.write("Welcome to my site.");
		out.flush();

		// クライアントとの接続を切断
		connect.close();

	}
} finally {
	if (server != null) {
		// サーバーを終了
		server.close();
	}
}
