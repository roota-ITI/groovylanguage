// "/bin/ls -l /var/log" というコマンドラインをGroovy scriptから起動し、
// コマンドの出力を、 "output.txt" というファイルに出力してください。  
// コマンドの出力を受け取り、ファイルに出力する処理もGroovyで記述します。  

// とりあえず、コマンドの実行結果を出力
//"/bin/ls -l /var/log".execute().text.eachLine{ println it }

// ファイルに出力(ってこう言うこと？)
//out = new File('./output.txt')
//"/bin/ls -l /var/log".execute().text.eachLine{ out.append(it+"\n") }

command = "/bin/ls -l /var/log"

p = command.execute()
// 終了まで待つ
p.waitFor()
// 終了コード
println 'return code = ' + p.exitValue()

// ファイルに出力
out = new File('./output.txt')
out.append(p.text)

// コマンドの出力を取得
// 標準出力
//println p.text
//println p.in.text
// 標準エラー出力
//println p.err.text
