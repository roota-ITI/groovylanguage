//  実行形式: groovy copyconv.groovy A B
//  file A の内容を file B にコピーする
//  "Java" という文字列は "Groovy" に変換する

src=args[0]
dest=args[1]

srcFile = new File(src)
destFile = new File(dest)
srcFile.eachLine{ destFile.append(it.replaceAll('Java','Groovy')+"\n") }
